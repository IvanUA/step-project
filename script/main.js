// removing default scroll up after click on a ellement from class: four-options
document.addEventListener('DOMContentLoaded', function() {
  const element = document.querySelector('.four-options');

  element.addEventListener('click', function(event) {
    event.preventDefault();  
  });
});

// removing default scroll up after click on a ellement from class: subtitles
document.addEventListener('DOMContentLoaded', function() {
  const element = document.querySelector('.subtitles');

  element.addEventListener('click', function(event) {
    event.preventDefault();  
  });
});
  
// removing default scroll up after click on a ellement from class: menu-work-section
document.addEventListener('DOMContentLoaded', function() {
  const element = document.querySelector('.menu-work-section');

  element.addEventListener('click', function(event) {
    event.preventDefault();  
  });
});

// removing default scroll up after click on a ellement from class: images-section
document.addEventListener('DOMContentLoaded', function() {
  const element = document.querySelector('.images-section');

  element.addEventListener('click', function(event) {
    event.preventDefault();  
  });
});

// removing default scroll up after click on a ellement from class:breaking-news-wrapper
document.addEventListener('DOMContentLoaded', function() {
  const element = document.querySelector('.breaking-news-wrapper');

  element.addEventListener('click', function(event) {
    event.preventDefault();  
  });
});

  // adding feature which shows exact information after click on a ellement from 
  // class subtitles
  function showTabContent(tabNumber) {
    // Hide all tab content
    var tabContents = document.getElementsByClassName("tab-content");
    for (var i = 0; i < tabContents.length; i++) {
      tabContents[i].classList.remove("active");
    }
    
    // Show the selected tab content
    var selectedTabContent = document.getElementById("tabContent" + tabNumber);
    selectedTabContent.classList.add("active");
  }
 
// slick slider
  $('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider',
    dots: false,
    centerMode: true,
    focusOnSelect: true
  });

  // Button which adding images in class work container with 2 seconds delay
  // Array of image URLs
const imageUrls = [
  './images/images_section_item1.jpg',
  './images/images_section_item2.jpg',
  './images/images_section_item3.jpg',
  './images/images_section_item4.jpg',
  './images/images_section_item5.jpg',
  './images/images_section_item6.jpg',
  './images/images_section_item7.jpg',
  './images/images_section_item8.jpg',
  './images/images_section_item9.jpg',
  './images/images_section_item10.jpg',
  './images/images_section_item11.jpg',
  './images/images_section_item5.jpg',
  './images/images_section_item1.jpg',
  './images/images_section_item2.jpg',
  './images/images_section_item3.jpg',
  './images/images_section_item4.jpg',
  './images/images_section_item5.jpg',
  './images/images_section_item6.jpg',
  './images/images_section_item7.jpg',
  './images/images_section_item8.jpg',
  './images/images_section_item9.jpg',
  './images/images_section_item10.jpg',
  './images/images_section_item11.jpg',
  './images/images_section_item5.jpg',];

const container = document.getElementById('image-container');
const button = document.getElementById('load-more-button');
const imagesPerLoad = 12;
const availableClasses = ['image-item wordpress', 'image-item landing', 'image-item web', 'image-item graphic'];

let currentImageIndex = 0;

function loadMoreImages() {
  const endIndex = currentImageIndex + imagesPerLoad;
  for (let i = currentImageIndex; i < endIndex && i < imageUrls.length; i++) {
    
    const img = document.createElement('img');
    const randomClass = availableClasses[Math.floor(Math.random() * availableClasses.length)];
    
    img.className = randomClass;
    img.src = imageUrls[i];
    setTimeout(function(){
      container.appendChild(img);
    },2000)
  }
  currentImageIndex = endIndex;

  // Hide the button if all images have been loaded
   
  if (currentImageIndex >= imageUrls.length) {
    button.style.display = 'none';
  }
}

button.addEventListener('click', loadMoreImages);

// adding images filter in work section
$(document).ready(function(){
  $('.menu-work-section-item').click(function(){
    const value = $(this).attr('data-filter');
    if (value == 'all') {
      $('.image-item').show('1000');
    } else {
      $('.image-item').not('.'+value).hide('1000');
      $('.image-item').filter('.'+value).show('1000');
    }
  })
  // add active class on selected item
  $('.menu-work-section-item').click(function(){
    $(this).addClass('active').siblings().removeClass('active');
  })
})

// Button which adding images in class Gallery with 2 seconds delay
  // Array of image URLs
  const imageUrlsGallery = [
    './images/images_section_item1.jpg',
    './images/images_section_item2.jpg',
    './images/images_section_item3.jpg',
    './images/images_section_item4.jpg',
    './images/images_section_item5.jpg',
    './images/images_section_item6.jpg',
    './images/images_section_item7.jpg',
    './images/images_section_item8.jpg',
    './images/images_section_item9.jpg',
    './images/images_section_item10.jpg',
    './images/images_section_item11.jpg',
    './images/images_section_item5.jpg',
    './images/images_section_item1.jpg',
    './images/images_section_item2.jpg',
    './images/images_section_item3.jpg',
    './images/images_section_item4.jpg',];
  
  const containerGallery = document.getElementById('gallary-images-container');
  const buttonGallery = document.getElementById('gallery-load-more-button');
  const imagesPerLoadGallery = 8;
  let currentImageIndexGallery = 0;
  
  
  function loadMoreImagesGallery() {
    const endIndex = currentImageIndexGallery  + imagesPerLoadGallery;
    for (let i = currentImageIndexGallery ; i < endIndex && i < imageUrlsGallery.length; i++) {
      
      const img = document.createElement('img');
      img.src = imageUrlsGallery[i];
      setTimeout(function(){
        containerGallery.appendChild(img);
      },2000)
    }
    currentImageIndexGallery  = endIndex;
    // Hide the button if all images have been loaded
    
    if (currentImageIndexGallery  >= imageUrlsGallery.length) {
      Galleryn.style.display = 'none';
    }
  }
  
  buttonGallery.addEventListener('click', loadMoreImagesGallery);
  

 
   